package main

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/nordic-coder/training/week1-exercise/handler"
)

func main() {
	// Echo instance
	e := echo.New()

	// Routes
	e.GET("/hello", handler.Hello)

	// Start server
	e.Start(":1323")
}
